import ctypes
import numpy as np
import cv2

_lib = ctypes.CDLL('src/lib_sscnet.so')
_lib.ReadVoxLabel.argtypes = (ctypes.c_char_p,
                              ctypes.c_void_p,
                              ctypes.c_void_p,
                              ctypes.c_void_p,
                              ctypes.c_void_p,
                              ctypes.c_void_p
                              )

_lib.ComputeTSDF.argtypes = (ctypes.c_void_p,
                              ctypes.c_void_p,
                              ctypes.c_void_p,
                              ctypes.c_void_p,
                              ctypes.c_void_p,
                              ctypes.c_void_p  )

_lib.FlipTSDF.argtypes = (ctypes.c_void_p,
                              ctypes.c_void_p)


_lib.SaveVoxLabel2Ply.argtypes = (ctypes.c_char_p,
                              ctypes.c_void_p,
                              ctypes.c_int,
                              ctypes.c_void_p)


def read_vox_label(filename, voxel_shape):
    global _lib


    vox_origin = np.ones(3,dtype=np.float32)
    cam_pose = np.ones(16,dtype=np.float32)
    num_voxels = voxel_shape[0] * voxel_shape[1] * voxel_shape[2]
    occupancy_label_fullsize = np.zeros(num_voxels, dtype=np.float32)
    segmentation_class_map = np.array([0, 1, 2, 3, 4, 11, 5, 6, 7, 8, 8, 10, 10, 10, 11, 11, 9, 8, 11, 11,
                                       11, 11, 11, 11, 11, 11, 11, 10, 10, 11, 8, 10, 11, 9, 11, 11, 11],dtype=np.int32)
    segmentation_label_fullsize = np.zeros(num_voxels, dtype=np.float32)

    _lib.ReadVoxLabel(ctypes.c_char_p(filename),
                      vox_origin.ctypes.data_as(ctypes.c_void_p),
                      cam_pose.ctypes.data_as(ctypes.c_void_p),
                      occupancy_label_fullsize.ctypes.data_as(ctypes.c_void_p),
                      segmentation_class_map.ctypes.data_as(ctypes.c_void_p),
                      segmentation_label_fullsize.ctypes.data_as(ctypes.c_void_p)
                      )
    return vox_origin, cam_pose, occupancy_label_fullsize, segmentation_label_fullsize


def get_tsdf_voxels(filename, voxel_shape, vox_origin, cam_pose):

    global _lib


    num_voxels = voxel_shape[0] * voxel_shape[1] * voxel_shape[2]
    vox_size = np.array([voxel_shape[0], voxel_shape[1], voxel_shape[2]], dtype=np.int32)
    vox_grid = np.zeros(num_voxels, dtype=np.float32)
    vox_tsdf = np.zeros(num_voxels, dtype=np.float32)

    depth_image = cv2.imread(filename, cv2.IMREAD_ANYDEPTH)

    _lib.ComputeTSDF(cam_pose.ctypes.data_as(ctypes.c_void_p),
                     vox_size.ctypes.data_as(ctypes.c_void_p),
                     vox_origin.ctypes.data_as(ctypes.c_void_p),
                     depth_image.ctypes.data_as(ctypes.c_void_p),
                     vox_grid.ctypes.data_as(ctypes.c_void_p),
                     vox_tsdf.ctypes.data_as(ctypes.c_void_p) )
    return depth_image, vox_grid, vox_tsdf


def flip_tsdf_voxels(vox_tsdf, voxel_shape):

    global _lib

    vox_size = np.array([voxel_shape[0], voxel_shape[1], voxel_shape[2]], dtype=np.int32)

    _lib.FlipTSDF(vox_size.ctypes.data_as(ctypes.c_void_p),
                  vox_tsdf.ctypes.data_as(ctypes.c_void_p) )
    return vox_tsdf


def save_vox_label_2_ply(filename, voxel_shape, segmentation_label_fullsize):

    vox_size = np.array([voxel_shape[0], voxel_shape[1], voxel_shape[2]], dtype=np.int32)
    label_downscale = 8
    _lib.SaveVoxLabel2Ply(ctypes.c_char_p(filename),
                      vox_size.ctypes.data_as(ctypes.c_void_p),
                      ctypes.c_int(label_downscale),
                      segmentation_label_fullsize.ctypes.data_as(ctypes.c_void_p)
                      )

def downsample(in_voxel_shape, out_voxel_shape, in_voxels):
    num_out_voxels = out_voxel_shape[0] * out_voxel_shape[1] * out_voxel_shape[2]

    in_vox_size = np.array([in_voxel_shape[0], in_voxel_shape[1], in_voxel_shape[2]], dtype=np.int32)
    out_vox_size = np.array([out_voxel_shape[0], out_voxel_shape[1], out_voxel_shape[2]], dtype=np.int32)
    out_voxels = np.zeros(num_out_voxels, dtype=np.float32)

    _lib.DownsampleLabel( in_vox_size.ctypes.data_as(ctypes.c_void_p),
                          out_vox_size.ctypes.data_as(ctypes.c_void_p),
                          in_voxels.ctypes.data_as(ctypes.c_void_p),
                          out_voxels.ctypes.data_as(ctypes.c_void_p)
                        )

    return  out_voxels
