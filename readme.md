# Projeto Livre - Complementação Semântica de Cenas 3D

![teaser](images/teaser.png)

# Instruções

## Conteúdo
0. [Requisitos](##Requisitos)
0. [Estrutura de Pastas](##Estrutura de Pastas)
0. [Instruções de Compilação](##Compilacao)
0. [Instruções de Execução](##Execucao)

## Requisitos
1. Ubuntu 16.04 LTS ou Superior
2. Python 3.4 (numpy, matplotlib, Types)
3. OpenCV 3


## Estrutura de Pastas
O projeto está organizado da seguinte forma:

```Shell
    |--data                    #data directory
       |--SUNCGtrain_1_500c    #partial training data
          |--*.bin             #ground truth 3d
          |--*.png             #depth image                 
    |--src                     #c++ source files
       libsscnet.cpp           #pre-processing shared library source code 
    |--images                  #result images directory
        |--depth*.png          #depth files
        |--depth_vox*.png      #snapshot of depth grids plots
        |--gt*.png             #snapshot of ground truth plots
        |--tsdf*.png           #snapshot of tsdf encoding plots
        |--f-tsdf*.png         #snapshot of f-tsdf enconding plots
    lib_sscnet.py              #python interface of pre-processing library
    test_lib.py                #pre-processing and ploting test script   
    voxel_plot.py              #3d ploting tools 
```


## Instruções de Compilação

```Shell
cd src
g++ -fPIC -shared -o lib_sscnet.so lib_sscnet.cpp
```

## Instruções de Execução

Para executar o script de teste digite:


```Shell
python test_lib.py
```

Após digitar a instrução acima, siga as instruçoes apresentadas no terminal.

Você pode obter outras imagens de profundidade e volumes no gitlab do projeto:
https://gitlab.com/aloisio/vc_3d.git


Para alterar a imagem utilizada nos testes, edite as linhas abaixo, no script test_lib.py:

```Shell
filename=b"data/SUNCGtrain_1_500/00006407_00298efe1bfeead6b172f25f0386b23a_fl002_rm0012_0000.bin"
fpng="data/SUNCGtrain_1_500/00006407_00298efe1bfeead6b172f25f0386b23a_fl002_rm0012_0000.png"
```

Os arquivos bin e png devem ser correspondentes.
