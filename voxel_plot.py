import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

def plot_depth_grid(vox_grid, voxel_shape):

    x = []
    y = []
    z = []

    for i in range(int(voxel_shape[0] * voxel_shape[1] * voxel_shape[2])):

        if (vox_grid[i] >0):
            zz = np.floor(i / (voxel_shape[0] * voxel_shape[1]))
            yy = np.floor((i - (zz * voxel_shape[0] * voxel_shape[1])) / voxel_shape[0])
            xx = i - (zz * voxel_shape[0] * voxel_shape[1]) - (yy * voxel_shape[0])

            z.append(-int(zz))
            y.append(int(yy))
            x.append(-int(xx))

    fig = plt.figure()

    ax = fig.add_subplot(111, projection='3d')
    plt.title("3d Depth")

    ax.scatter(x, y, z, c="gray", s=2)
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    ax.view_init(azim=90, elev=-80)
    plt.draw()
    plt.show()

def plot_ground_truth(vox_gt, voxel_shape):

    x = []
    y = []
    z = []
    label = []

    for i in range(int(voxel_shape[0] * voxel_shape[1] * voxel_shape[2])):

        if (vox_gt[i] >0 and vox_gt[i] <255):
            zz = np.floor(i / (voxel_shape[0] * voxel_shape[1]))
            yy = np.floor((i - (zz * voxel_shape[0] * voxel_shape[1])) / voxel_shape[0])
            xx = i - (zz * voxel_shape[0] * voxel_shape[1]) - (yy * voxel_shape[0])

            z.append(-int(zz))
            y.append(int(yy))
            x.append(-int(xx))

            label.append(vox_gt[i])

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    plt.title("Ground Truth")

    ax.scatter(x, y, z, c=label, s=2, cmap=plt.cm.get_cmap('tab10'))
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    ax.view_init(azim=90, elev=-80)
    plt.draw()
    plt.show()


def plot_tsdf(vox_tsdf, voxel_shape):
    x = []
    y = []
    z = []
    tsdf = []

    for i in range(int(voxel_shape[0] * voxel_shape[1] * voxel_shape[2])):

        if (vox_tsdf[i] != 1.0 and vox_tsdf[i] != 0.0 and vox_tsdf[i] != -1.0):
            zz = np.floor(i / (voxel_shape[0] * voxel_shape[1]))
            yy = np.floor((i - (zz * voxel_shape[0] * voxel_shape[1])) / voxel_shape[0])
            xx = i - (zz * voxel_shape[0] * voxel_shape[1]) - (yy * voxel_shape[0])

            z.append(-int(zz))
            y.append(int(yy))
            x.append(-int(xx))

            tsdf.append(vox_tsdf[i])

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    plt.title("TSDF")

    ax.scatter(x, y, z, c=tsdf, s=2, cmap=plt.cm.get_cmap('RdYlBu'), vmin=-1, vmax=1)
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    ax.view_init(azim=90, elev=-80)
    plt.draw()
    plt.show()

def plot_flipped_tsdf(vox_tsdf, voxel_shape):
    x = []
    y = []
    z = []
    tsdf = []

    for i in range(int(voxel_shape[0] * voxel_shape[1] * voxel_shape[2])):

        if ((vox_tsdf[i] > -.9 and vox_tsdf[i] < -0.002) or  (vox_tsdf[i] > 0.002 and vox_tsdf[i] < 0.9)):
            zz = np.floor(i / (voxel_shape[0] * voxel_shape[1]))
            yy = np.floor((i - (zz * voxel_shape[0] * voxel_shape[1])) / voxel_shape[0])
            xx = i - (zz * voxel_shape[0] * voxel_shape[1]) - (yy * voxel_shape[0])

            z.append(-int(zz))
            y.append(int(yy))
            x.append(-int(xx))

            tsdf.append(-vox_tsdf[i])

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    plt.title("Flipped TSDF")

    ax.scatter(x, y, z, c=tsdf, s=2, cmap=plt.cm.get_cmap('RdYlBu'), vmin=-1, vmax=1)
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    ax.view_init(azim=90, elev=-80)
    plt.draw()
    plt.show()


