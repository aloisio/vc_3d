# Test script for lib_sccnet

from lib_sscnet import read_vox_label,  get_tsdf_voxels,  flip_tsdf_voxels
from voxel_plot import *
import cv2
import numpy as np


#filename=b"data/SUNCGtrain_1_500/00006407_00298efe1bfeead6b172f25f0386b23a_fl002_rm0012_0000.bin"
#fpng="data/SUNCGtrain_1_500/00006407_00298efe1bfeead6b172f25f0386b23a_fl002_rm0012_0000.png"

#filename=b"data/SUNCGtrain_1_500/00000009_01d4be86806197794c9333540d5bf77c_fl001_rm0007_0000.bin"
#fpng="data/SUNCGtrain_1_500/00000009_01d4be86806197794c9333540d5bf77c_fl001_rm0007_0000.png"

#filename=b"data/SUNCGtrain_1_500/00000016_01d4be86806197794c9333540d5bf77c_fl001_rm0007_0000.bin"
#fpng="data/SUNCGtrain_1_500/00000016_01d4be86806197794c9333540d5bf77c_fl001_rm0007_0000.png"

filename=b"data/SUNCGtrain_1_500/00000043_015394e050ffbd1878db1f7d1a70842f_fl001_rm0006_0000.bin"
fpng="data/SUNCGtrain_1_500/00000043_015394e050ffbd1878db1f7d1a70842f_fl001_rm0006_0000.png"

voxel_shape = (240, 144, 240)

print("Reading RLE encoded Ground Truth...")
vox_origin, cam_pose, occupancy_label, vox_label = read_vox_label(filename, voxel_shape)

print("plot_ground_truth, close the view window to continue...")
plot_ground_truth(vox_label, voxel_shape)

print("Reading and voxelizing PNG depth...")
depth_image, vox_grid, vox_tsdf = get_tsdf_voxels(fpng, voxel_shape, vox_origin, cam_pose)

print("plot_depth_grid, close the view window to continue...")
plot_depth_grid(vox_grid, voxel_shape)

cv2.imshow('Depth Image', depth_image)
print("Click at the Depth Image and press any key to continue...")
cv2.waitKey(0)

print("plot_tsdf, close the view window to continue...")
plot_tsdf(vox_tsdf, voxel_shape)


print("Getting Flipped TSDF...")
flip_tsdf_voxels(vox_tsdf, voxel_shape)

print("plot_flipped_tsdf, close the view window to continue...")
plot_flipped_tsdf(vox_tsdf, voxel_shape)


