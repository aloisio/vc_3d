/*
    SSCNET Data preparation functions extracted from original Caffe code
    Adapted to use with Python and numpy
    Author: Aloísio Dourado (jun, 2018)
    Original Caffe Code: Shuran Song (https://github.com/shurans/sscnet)
*/

#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include <string.h>
using namespace std;

// Camera information
int frame_width = 640; // in pixels
int frame_height = 480;
float vox_unit = 0.02;
float vox_margin = 0.24;

float cam_K[9] = {518.8579f, 0.0f, (float)frame_width / 2.0f, 0.0f, 518.8579f, (float)frame_height / 2.0f, 0.0f, 0.0f, 1.0f};
float cam_info[27];


float modeLargerZero(const std::vector<float>& values) {
  float old_mode = 0;
  float old_count = 0;
  for (size_t n = 0; n < values.size(); ++n) {
    if (values[n] > 0 && values[n] < 255) {
      float mode = values[n];
      float count = std::count(values.begin() + n + 1, values.end(), mode);

      if (count > old_count) {
        old_mode = mode;
        old_count = count;
      }
    }
  }
  return old_mode;
}

// find mode of in an vector
float mode(const std::vector<float>& values) {
  float old_mode = 0;
  float old_count = 0;
  for (size_t n = 0; n < values.size(); ++n) {
    float mode = values[n];
    float count = std::count(values.begin() + n + 1, values.end(), mode);

    if (count > old_count) {
      old_mode = mode;
      old_count = count;
    }
  }
  return old_mode;
}


void ReadVoxLabel_CPP(const std::string &filename, float *vox_origin,
                  float *cam_pose, float *occupancy_label_fullsize,
                  int *segmentation_class_map,
                  float *segmentation_label_fullsize) {

  cout << "\nReadVoxLabel_CPP\n";


  // Open file
  std::ifstream fid(filename, std::ios::binary);

  // Read voxel origin in world coordinates
  for (int i = 0; i < 3; ++i) {
    fid.read((char*)&vox_origin[i], sizeof(float));
  }

  // Read camera pose
  for (int i = 0; i < 16; ++i) {
    fid.read((char*)&cam_pose[i], sizeof(float));
  }

  // Read voxel label data from file (RLE compression)
  std::vector<unsigned int> scene_vox_RLE;
  while (!fid.eof()) {
    int tmp;
    fid.read((char*)&tmp, sizeof(int));
    if (!fid.eof())
      scene_vox_RLE.push_back(tmp);
  }

  // Reconstruct voxel label volume from RLE
  int vox_idx = 0;
  for (size_t i = 0; i < scene_vox_RLE.size() / 2; ++i) {
    unsigned int vox_val = scene_vox_RLE[i * 2];
    unsigned int vox_iter = scene_vox_RLE[i * 2 + 1];
    for (size_t j = 0; j < vox_iter; ++j) {
      if (vox_val == 255) {
        segmentation_label_fullsize[vox_idx] = float(255);
        occupancy_label_fullsize[vox_idx] = float(0.0f);
      } else {
        segmentation_label_fullsize[vox_idx] = float(
            segmentation_class_map[vox_val]);
            //segmentation_label_fullsize[vox_idx] = float(vox_val);
        if (vox_val > 0)
          occupancy_label_fullsize[vox_idx] = float(1.0f);
        else
          occupancy_label_fullsize[vox_idx] = float(0.0f);
      }
      vox_idx++;
    }
  }

}

// Save voxel volume labels to point cloud ply file for visualization
void SaveVoxLabel2Ply_CPP(const std::string &filename, int *vox_size, int label_downscale, float *vox_label) {



  // Count total number of points in point cloud
  int num_points = 0;
  for (int i = 0; i < vox_size[0] * vox_size[1] * vox_size[2]; ++i)
    if (vox_label[i] > 0)
      num_points++;

  // Create header for ply file
  FILE *fp = fopen(filename.c_str(), "w");
  fprintf(fp, "ply\n");
  fprintf(fp, "format binary_little_endian 1.0\n");
  fprintf(fp, "element vertex %d\n", num_points);
  fprintf(fp, "property float x\n");
  fprintf(fp, "property float y\n");
  fprintf(fp, "property float z\n");
  fprintf(fp, "property uchar red\n");
  fprintf(fp, "property uchar green\n");
  fprintf(fp, "property uchar blue\n");
  fprintf(fp, "end_header\n");

  // Create different colors for each class
  const int num_classes = 36;
  int class_colors[num_classes * 3];
  for (int i = 0; i < num_classes; ++i) {
    class_colors[i * 3 + 0] = (int)(round(rand()%200)+55);
    class_colors[i * 3 + 1] = (int)(round(rand()%200)+55);
    class_colors[i * 3 + 2] = (int)(round(rand()%200)+55);
  }

  // Create point cloud content for ply file
  for (int i = 0; i < vox_size[0] * vox_size[1] * vox_size[2]; ++i) {

    // If class of voxel non-empty, add voxel coordinates to point cloud
    if (vox_label[i] > 0) {

      // Compute voxel indices in int for higher positive number range
      int z = floor(i / (vox_size[0] * vox_size[1]));
      int y = floor((i - (z * vox_size[0] * vox_size[1])) / vox_size[0]);
      int x = i - (z * vox_size[0] * vox_size[1]) - (y * vox_size[0]);

      // Convert voxel indices to float, and save coordinates to ply file
      float float_x = (float)x * (float)label_downscale + (float)label_downscale / 2;
      float float_y = (float)y * (float)label_downscale + (float)label_downscale / 2;
      float float_z = (float)z * (float)label_downscale + (float)label_downscale / 2;
      fwrite(&float_x, sizeof(float), 1, fp);
      fwrite(&float_y, sizeof(float), 1, fp);
      fwrite(&float_z, sizeof(float), 1, fp);

      // Save color of class into voxel
      unsigned char color_r = (unsigned char) class_colors[(int)vox_label[i] * 3 + 0];
      unsigned char color_g = (unsigned char) class_colors[(int)vox_label[i] * 3 + 1];
      unsigned char color_b = (unsigned char) class_colors[(int)vox_label[i] * 3 + 2];
      fwrite(&color_r, sizeof(unsigned char), 1, fp);
      fwrite(&color_g, sizeof(unsigned char), 1, fp);
      fwrite(&color_b, sizeof(unsigned char), 1, fp);
    }
  }
  fclose(fp);
}

void DownsampleLabel_CPP(
    int *in_vox_size, int *out_vox_size,
    float *in_voxels,
    float *out_voxels) {

  int label_downscale = (int) in_vox_size[0] / out_vox_size[0];
  float emptyT = (0.95 * label_downscale * label_downscale * label_downscale);
  for (int i = 0;
       i < out_vox_size[0] * out_vox_size[1] * out_vox_size[2]; ++i) {
    int z = floor(i / (out_vox_size[0] * out_vox_size[1]));
    int y = floor(
        (i - (z * out_vox_size[0] * out_vox_size[1])) / out_vox_size[0]);
    int x = i - (z * out_vox_size[0] * out_vox_size[1]) -
            (y * out_vox_size[0]);

    std::vector<float> field_vals;
    std::vector<float> tsdf_vals;
    int zero_count = 0;
    for (int tmp_x = x * label_downscale;
         tmp_x < (x + 1) * label_downscale; ++tmp_x) {
      for (int tmp_y = y * label_downscale;
           tmp_y < (y + 1) * label_downscale; ++tmp_y) {
        for (int tmp_z = z * label_downscale;
             tmp_z < (z + 1) * label_downscale; ++tmp_z) {
          int tmp_vox_idx = tmp_z * in_vox_size[0] * in_vox_size[1] +
                            tmp_y * in_vox_size[0] + tmp_x;
          field_vals.push_back(in_voxels[tmp_vox_idx]);
          if (in_voxels[tmp_vox_idx] < float(0.001f) || in_voxels[tmp_vox_idx] > float(254)) {
            zero_count++;
          }
        }
      }
    }

    if (zero_count > emptyT) {
      out_voxels[i] = float(mode(field_vals));
    } else {
      out_voxels[i] = float(modeLargerZero(field_vals)); // object label mode without zeros
    }
  }

}

void depth2Grid(float *cam_pose, int *vox_size,  float *vox_origin, float *depth_data, float *vox_grid){

  // Get point in world coordinate
  // Try to parallel later

  for (int pixel_x=0; pixel_x<frame_width; pixel_x++) {

      //cout << " ";
      //cout << pixel_x;


      for (int pixel_y=0; pixel_y<frame_height; pixel_y++) {

          float point_depth = depth_data[pixel_y * frame_width + pixel_x];

          float point_cam[3] = {0};
          point_cam[0] =  (pixel_x - cam_K[2])*point_depth/cam_K[0];
          point_cam[1] =  (pixel_y - cam_K[5])*point_depth/cam_K[4];
          point_cam[2] =  point_depth;

          float point_base[3] = {0};

          point_base[0] = cam_pose[0 * 4 + 0]* point_cam[0] + cam_pose[0 * 4 + 1]*  point_cam[1] + cam_pose[0 * 4 + 2]* point_cam[2];
          point_base[1] = cam_pose[1 * 4 + 0]* point_cam[0] + cam_pose[1 * 4 + 1]*  point_cam[1] + cam_pose[1 * 4 + 2]* point_cam[2];
          point_base[2] = cam_pose[2 * 4 + 0]* point_cam[0] + cam_pose[2 * 4 + 1]*  point_cam[1] + cam_pose[2 * 4 + 2]* point_cam[2];

          point_base[0] = point_base[0] + cam_pose[0 * 4 + 3];
          point_base[1] = point_base[1] + cam_pose[1 * 4 + 3];
          point_base[2] = point_base[2] + cam_pose[2 * 4 + 3];


          //printf("vox_origin: %f,%f,%f\n",vox_origin[0],vox_origin[1],vox_origin[2]);
          // World coordinate to grid coordinate
          int z = (int)floor((point_base[0] - vox_origin[0])/vox_unit);
          int x = (int)floor((point_base[1] - vox_origin[1])/vox_unit);
          int y = (int)floor((point_base[2] - vox_origin[2])/vox_unit);
          //printf("point_base: %f,%f,%f, %d,%d,%d, %d,%d,%d \n",point_base[0],point_base[1],point_base[2], z, x, y, vox_size[0],vox_size[1],vox_size[2]);

          // mark vox_out with 1.0
          if( x >= 0 && x < vox_size[0] && y >= 0 && y < vox_size[1] && z >= 0 && z < vox_size[2]){
              int vox_idx = z * vox_size[0] * vox_size[1] + y * vox_size[0] + x;
              vox_grid[vox_idx] = float(1.0);
          }
      }
  }
}

void SquaredDistanceTransform(float *cam_pose, int *vox_size,  float *vox_origin, float *depth_data, float *vox_grid, float *vox_tsdf) {

  // Try to paralelize later
    printf("\nSquaredDistanceTransform\n");
    printf("\nIt will take some time, sorry!");
    printf("\nI need to parallelize this code or put it in GPU!");
    printf("\nGrab a coffe and wait until z=244\n");

  for (int z = 0; z < vox_size[2]; ++z) {

        if (z%10==0)
            printf("z=%d\n",z);

      for (int y = 0; y < vox_size[1]; ++y) {

          int search_region = (int)round(vox_margin/vox_unit);

          for (int x = 0; x < vox_size[0]; ++x) {
            int vox_idx = z * vox_size[0] * vox_size[1] + y * vox_size[0] + x;

            //vox_tsdf[vox_idx] = 1.0 - vox_binary_GPU[vox_idx];

            // Get point in world coordinates XYZ -> YZX
            float point_base[3] = {0};
            point_base[0] = float(z) * vox_unit + vox_origin[0];
            point_base[1] = float(x) * vox_unit + vox_origin[1];
            point_base[2] = float(y) * vox_unit + vox_origin[2];

            // Encode height from floor ??? check later

            // Get point in current camera coordinates
            float point_cam[3] = {0};
            point_base[0] = point_base[0] - cam_pose[0 * 4 + 3];
            point_base[1] = point_base[1] - cam_pose[1 * 4 + 3];
            point_base[2] = point_base[2] - cam_pose[2 * 4 + 3];
            point_cam[0] = cam_pose[0 * 4 + 0] * point_base[0] + cam_pose[1 * 4 + 0] * point_base[1] + cam_pose[2 * 4 + 0] * point_base[2];
            point_cam[1] = cam_pose[0 * 4 + 1] * point_base[0] + cam_pose[1 * 4 + 1] * point_base[1] + cam_pose[2 * 4 + 1] * point_base[2];
            point_cam[2] = cam_pose[0 * 4 + 2] * point_base[0] + cam_pose[1 * 4 + 2] * point_base[1] + cam_pose[2 * 4 + 2] * point_base[2];
            if (point_cam[2] <= 0)
              continue;

            // Project point to 2D
            int pixel_x = roundf(cam_K[0] * (point_cam[0] / point_cam[2]) + cam_K[2]);
            int pixel_y = roundf(cam_K[4] * (point_cam[1] / point_cam[2]) + cam_K[5]);
            if (pixel_x < 0 || pixel_x >= frame_width || pixel_y < 0 || pixel_y >= frame_height){ // outside FOV
              //vox_tsdf[vox_idx] = GPUCompute2StorageT(-1.0);
              continue;
            }

            // Get depth
            float point_depth = depth_data[pixel_y * frame_width + pixel_x];
            if (point_depth < float(0.0f) || point_depth > float(10.0f))
              continue;
            if (roundf(point_depth) == 0){ // mising depth
              vox_tsdf[vox_idx] = -1.0;
              continue;
            }

            // Get depth difference
            float point_dist = (point_depth - point_cam[2]) * sqrtf(1 + powf((point_cam[0] / point_cam[2]), 2) + powf((point_cam[1] / point_cam[2]), 2));
            float sign = point_dist/abs(point_dist);
            vox_tsdf[vox_idx] = sign;
            //vox_tsdf[vox_idx] = sign*fmin(1, abs(point_dist)/vox_margin);
            if (abs(point_dist) < 2 * vox_margin){
              for (int iix = max(0,x-search_region); iix < min((int)vox_size[0],x+search_region+1); iix++){
                for (int iiy = max(0,y-search_region); iiy < min((int)vox_size[1],y+search_region+1); iiy++){
                  for (int iiz = max(0,z-search_region); iiz < min((int)vox_size[2],z+search_region+1); iiz++){
                    int iidx = iiz * vox_size[0] * vox_size[1] + iiy * vox_size[0] + iix;
                    if (vox_grid[iidx] > 0){
                      float xd = abs(x - iix);
                      float yd = abs(y - iiy);
                      float zd = abs(z - iiz);
                      float tsdf_value = (sqrtf(xd * xd + yd * yd + zd * zd)*vox_unit)/vox_margin;
                      //printf("%f, %f, %f, %f, %f\n",tsdf_value,abs(vox_tsdf[vox_idx]), xd,yd,zd);
                      if (tsdf_value < abs(vox_tsdf[vox_idx])){
                        vox_tsdf[vox_idx] = tsdf_value*sign;
                      }
                    }
                  }
                }
              }
            }
          }
      }
  }
  printf("\nDone!!!\n");

}


void ComputeTSDF_CPP(float *cam_pose, int *vox_size,  float *vox_origin, unsigned char *depth_image, float *vox_grid, float *vox_tsdf) {

  cout << "\nComputeTSDF_CPP\n";

  unsigned short depth_raw;
  float *depth_data = new float[frame_height * frame_width];

  for (int i = 0; i < frame_height * frame_width; ++i) {
    depth_raw = ((((unsigned short)depth_image[i * 2 + 1]) << 8) + ((unsigned short)depth_image[i * 2 + 0]));
    depth_raw = (depth_raw << 13 | depth_raw >> 3);
    depth_data[i] = float((float)depth_raw / 1000.0f);
  }

  int num_crop_voxels = vox_size[0] * vox_size[1] * vox_size[2];

  //float *vox_grid = new float[num_crop_voxels];
  //memset(vox_grid, 0, num_crop_voxels * sizeof(float));

  // from depth map to binaray voxel representation
  depth2Grid(cam_pose, vox_size,  vox_origin, depth_data, vox_grid);

  // distance transform
  SquaredDistanceTransform(cam_pose, vox_size, vox_origin, depth_data, vox_grid, vox_tsdf);
  //delete [] vox_grid;
  delete [] depth_data;

}

void ComputeOccupancyTSDF_CPP (int *vox_size, float *occupancy_label, float *vox_tsdf) {

    int search_region = (int)round(vox_margin/vox_unit);

    int sum_occupied = 0;
    int not_occupied = 0;

    cout << "\nComputeTSDF_CPP\n";
    printf("\nIt will take some time, sorry!");
    printf("\nI need to parallelize this code or put it in GPU!");
    printf("\nGrab a coffe and wait until z=244\n");


    for (int z = 0; z < vox_size[2]; ++z) {

        if (z%10==0)
            printf("z=%d\n",z);

        for (int y = 0; y < vox_size[1]; ++y)

            for (int x = 0; x < vox_size[0]; ++x) {

                int vox_idx = z * vox_size[0] * vox_size[1] + y * vox_size[0] + x;

                if (occupancy_label[vox_idx] >0 ) {

                    vox_tsdf[vox_idx] = -0.001;// inside mesh
                    sum_occupied++;
                }
                else {
                   vox_tsdf[vox_idx] = 1.0;// outside mesh
                   not_occupied++;
                    for (int iix = max(0,x-search_region); iix < min((int)vox_size[0],x+search_region+1); iix++){
                      for (int iiy = max(0,y-search_region); iiy < min((int)vox_size[1],y+search_region+1); iiy++){
                        for (int iiz = max(0,z-search_region); iiz < min((int)vox_size[2],z+search_region+1); iiz++){
                            int iidx = iiz * vox_size[0] * vox_size[1] + iiy * vox_size[0] + iix;
                            if (occupancy_label[iidx] > 0){
                                float xd = abs(x - iix);
                                float yd = abs(y - iiy);
                                float zd = abs(z - iiz);
                                float tsdf_value = sqrtf(xd * xd + yd * yd + zd * zd)/(float)search_region;
                                if (tsdf_value < abs(vox_tsdf[vox_idx])){
                                  vox_tsdf[vox_idx] = tsdf_value;
                                }
                            }
                        }
                      }
                    }
                }
            }
    }

    printf("\nDone!!!\n");
    cout << "\noccup ";
    cout << sum_occupied;
    cout << "\nnot occup ";
    cout << not_occupied;
    cout << "\n";

}

void FlipTSDF_CPP( int *vox_size, float *vox_tsdf){

  for (int vox_idx=0; vox_idx< vox_size[0]*vox_size[1]*vox_size[2]; vox_idx++) {

      float value = float(vox_tsdf[vox_idx]);


      float sign;
      if (abs(value) < 0.001)
        sign = 1;
      else
        sign = value/abs(value);

      vox_tsdf[vox_idx] = sign*(max(0.001,(1.0-abs(value))));
  }
}



extern "C" {
    void ReadVoxLabel(const char *filename, float *vox_origin,
                  float *cam_pose, float *occupancy_label_fullsize,
                  int *segmentation_class_map,
                  float *segmentation_label_fullsize) {
                                  ReadVoxLabel_CPP(filename, vox_origin,
                                                      cam_pose, occupancy_label_fullsize,
                                                      segmentation_class_map,
                                                      segmentation_label_fullsize);
                  }

    void SaveVoxLabel2Ply(const char *filename, int *vox_size, int label_downscale, float *vox_label) {
                                  SaveVoxLabel2Ply_CPP(filename, vox_size, label_downscale, vox_label);
                  }

    void DownsampleLabel(
                                int *in_vox_size, int *out_vox_size,
                                float *in_voxels,
                                float *out_voxels) {

                                            DownsampleLabel_CPP(
                                                in_vox_size, out_vox_size,
                                                in_voxels,
                                                out_voxels);
                                        }

    void ComputeTSDF(float *cam_pose, int *vox_size,  float *vox_origin, unsigned char *depth_data, float *vox_grid, float *vox_tsdf) {
                                  ComputeTSDF_CPP(cam_pose, vox_size,  vox_origin, depth_data, vox_grid, vox_tsdf);
                   }

    void ComputeOccupancyTSDF (int *vox_size, float *occupancy_label, float *vox_tsdf) {
                                  ComputeOccupancyTSDF_CPP (vox_size, occupancy_label, vox_tsdf);

    }

    void FlipTSDF( int *vox_size, float *vox_tsdf){
                                  FlipTSDF_CPP( vox_size, vox_tsdf);
    }
}